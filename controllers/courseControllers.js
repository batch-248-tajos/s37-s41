const Course = require("../models/Course");

// module.exports.addCourse = (reqBody) =>{

// 	let newCourse = new Course({

// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
	
// 	});

// 	return newCourse.save().then((course,error)=>{

// 		if (error){
// 			return false
// 		}else{
// 			return true
// 		}


// 	})

// }

module.exports.addCourse = (data) =>{
	if(data.isAdmin){

		let newCourse = new Course({

		name: data.course.name,
		description: data.course.description,
		price: data.course.price
	});

	return newCourse.save().then((course,error)=>{

		if (error){
			return false
		}else{
			return true
		}

	})

	}else{
		return false
	}
};

//Retrieve all courses
/*
	Steps:
	1. Retrieve all the courses from the database

*/

module.exports.getAllCourses = () =>{

	return Course.find({}).then(result=>{
		return result;
	});

};


//Retrive all Active Courses
/*
	
	Step:
	1. Retrieve all the courses from the db with the property "isActive" with the value true

*/

module.exports.getAllActive = () =>{

	return Course.find({isActive: true}).then(result=>{
		return result;
	});

};

//Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain information retrieved from the reqBody
	2. Find and update the course using the courseId retrieved from the reqParams property and the variable "updatedCourse" containing the information from the reqBody

*/

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price: reqBody.price
	}

	//findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=>{

		if(error){
			return false
		}else{
			return true;
		};
	});
};


//Archive a course
//In managing db, it is a common practice to soft delete our records and what we would implement in the "delete" operation of our application
/*
	1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
	2.Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
	Process a PATCH request at the /courseId/archive route using postman to archive a course

*/
module.exports.archiveCourse = (reqParams) => {
    return Course.findByIdAndUpdate(reqParams.courseId, {isActive: false})
        .then((archived, err) => {
            if (err) {
                return false
            }

            return true
        })
}

module.exports.unarchiveCourse = (reqParams) => {
    return Course.findByIdAndUpdate(reqParams.courseId, {isActive: true})
        .then((unarchived, err) => {
            if (err) {
                return false
            }

            return true
        })
}