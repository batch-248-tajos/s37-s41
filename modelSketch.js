/**
 * App: Booking System API
 * 
 * Scenario:
 *      A course booking system application where a user can enroll into a course
 * 
 * Type: Course Booking System (Web App)
 * 
 * Description:
 *      .....   
 * 
 * Features:
 *      .....
 * 
 * Data Model
 *      user {
 *          id - unique identifier
 *          firstName,
 *          lastName,
 *          email,
 *          password,
 *          mobileNumber,
 *          isAdmin
 *          enrollments: [
 *              courseId,
 *              enrolledOn,
 *              status
 *          ]
 *      }
 * 
 *      course {
 *          id,
 *          name,
 *          description,
 *          price,
 *          isActive,
 *          createdOn,
 *          enrollees: [
 *              id
 *              userId,
 *              courseId
 *          ]
 *      }
 */